<?php


use Illuminate\Support\Facades\Route;


Route::post('/login', [\vorax\Auth\Http\Controllers\AuthController::class, 'login']);
Route::post('/logout', [\vorax\Auth\Http\Controllers\AuthController::class, 'logout']);