<?php


namespace vorax\Auth;


use App\Models\User;
use Illuminate\Validation\Rule;


class AuthHandler
{

    private $token;


    public function generateToken()
    {

        $tokens = User::select('register_token')->get()->toArray();

        do {
            $this->token = \Str::random(64);
            $flag        = false;

            if (!in_array($this->token, $tokens)) {
                $flag = true;
                break;
            }
        } while (!$flag);

        return $this->token;
    }


    public function checkToken($token)
    {
        $user = User::where('register_token', $token)
            ->get();

        if (!$user)
            abort(403, 'the credential does not match with our data');

        return [
            'success' => true,
            'user'    => $user,
        ];
    }
}
