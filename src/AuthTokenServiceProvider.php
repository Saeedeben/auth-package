<?php


namespace vorax\Auth;


use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

class AuthTokenServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('AuthHandler', function ($app) {
            return new AuthHandler();
        });

        $this->mergeConfigFrom(__DIR__ . '/../config/auth_config.php', 'vorax');
    }

    public function boot()
    {
        $this->registerRoutes();

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__ . '/../config/auth_config.php' => config_path('auth_config.php'),
            ], 'config');
        }

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('authCheck', Http\Middleware\authCheckMiddleware::class);

    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
//            'middleware' => 'Http/Middleware/authCheckMiddleware',
        ];
    }
}
