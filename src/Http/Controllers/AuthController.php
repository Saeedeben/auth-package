<?php


namespace vorax\Auth\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use vorax\Auth\Facades\AuthHandler;
use App\Models\User;

class AuthController extends Controller
{

    private $token;
    private $username;
    private $usernameKind;

    public function __construct($token = NULL, $username = NULL, $usernameKind = NULL)
    {
        $this->token        = \request()->token;
        $this->username     = \request()->username;
        $this->usernameKind = \request()->usernameKind;
    }

    public function generateToken()
    {
        return AuthHandler::generateToken();
    }


    public function login(Request $request)
    {
        $user = User::where($this->usernameKind, $this->username);

        $password     = $request->password;
        $userPassword = $user->get()->pluck('password')->toArray();
        $hash         = Hash::check($password, $userPassword[0]);

        if (!$user || !$hash)
            return [
                'success' => false,
                'message' => 'کاربر با این اطلاعات وجود ندارد',
            ];
        $token = $this->generateToken();

        $user->update(['register_token' => $token]);

        return [
            'success' => true,
            'message' => 'شما با موفقیت وارد شدید',
            'token'   => $token,
        ];
    }

    public function logout()
    {
        if (!$this->token)
            abort(403);

        User::where('register_token', $this->token)
            ->update(['register_token' => NULL]);

        return [
            'success' => true,
            'message' => 'شما با موفقیت خارج شدید',
        ];

    }

    public function register()
    {

    }


}
