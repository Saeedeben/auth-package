<?php

namespace vorax\Auth\Http\Middleware;

use Closure;
use vorax\Auth\Facades\AuthHandler;

class authCheckMiddleware
{
    public function handle($request, Closure $next)
    {
        if ($request->bearerToken()) {
            $check = AuthHandler::checkToken($request->bearerToken());
            if ($check['success']) {
                return $next($request);
            }
        }

        abort(403);

    }
}
