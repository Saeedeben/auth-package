<?php


namespace vorax\Auth\Facades;


use Illuminate\Support\Facades\Facade;

class AuthHandler extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'AuthHandler';
	}
}
